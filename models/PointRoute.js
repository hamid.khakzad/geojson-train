const mongoose = require("mongoose");

const pointRoute = new mongoose.Schema({
  loc: {
    type: { type: String },
    coordinates: [],
  }
});

const PointRoute = mongoose.model('GeoItem', pointRoute);

module.exports= PointRoute;
