const express = require("express");
const router = express.Router();
const pointRoute = require("./models/PointRoute");


router.post("/points", async (req, res) => {
    let geoJsonType = req.body["item_type"];
    let coordinates = req.body.coordinates;
    let newItem;

    const data = [
        {loc: {type: 'Point', coordinates: [-20.0, 5.0]}},
    ];
    switch (geoJsonType) {
        case 'Point':
            newItem = new pointRoute({loc: {type: 'Point', coordinates: coordinates}});
            break;
        case 'Polygon':
          newItem = new pointRoute({loc: {type: 'Polygon', coordinates: coordinates}});
          break;
        case 'LineString':
          newItem = new pointRoute({loc: {type: 'LineString', coordinates: coordinates}});
          break;
    }
    await newItem.save().then(savedPoint => {
        res.json({message: "geo item saved success",id: savedPoint.id})
    })
});

router.get("/points/:id", async (req, res) => {
    try {
        const geoItem = await pointRoute.findOne({_id: req.params.id});
        res.send({item_type:geoItem.loc.type,coordinates: geoItem.loc.coordinates});
    } catch {
        res.status(404);
        res.send({error: "geo item doesn't exist!"});
    }
});


module.exports = router;
