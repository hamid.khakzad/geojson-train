const express = require("express");
const mongoose = require("mongoose");
const routes = require("./routes");
const pointRoute=require ("./models/PointRoute");

mongoose
  .connect("mongodb://localhost:27018/testdatavase", { useNewUrlParser: true })
  .then(async () => {

    const app = express();
    app.use(express.json());
    app.use("/api", routes);

    app.listen(5000, () => {
      console.log("Server has started!");
    });
  });
